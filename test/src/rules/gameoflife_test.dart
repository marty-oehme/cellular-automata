import 'package:rules_of_living/src/rules/GameOfLife.dart';
import 'package:test/test.dart';

void main() {
  GameOfLife sut;
  setUp(() {
    sut = GameOfLife();
  });
  group("BirthRules", () {
    test("will return true when being passed three neighbors",
        () => expect(sut.checkBirth(3), true));
    test("will return false when being passed zero neighbors",
        () => expect(sut.checkBirth(0), false));
    test("will return false when being passed two neighbors",
        () => expect(sut.checkBirth(2), false));
  });
  group("SurviveRules", () {
    test("will return true when being passed two neighbors",
        () => expect(sut.checkSurvival(2), true));
    test("will return true when being passed three neighbors",
        () => expect(sut.checkSurvival(3), true));
    test("will return false when being passed 0 neighbors",
        () => expect(sut.checkSurvival(0), false));
    test("will return false when being passed more than 3 neighbors",
        () => expect(sut.checkSurvival(4), false));
  });
}
