import 'dart:math';

import 'package:rules_of_living/src/Grid.dart';
import 'package:test/test.dart';

@Tags(const ["nobrowser"])
void main() {
  group("Instantiation", () {
    List<String> l;
    setUp(() {
      l = [
        "Hey",
        "you",
        "me",
        "together",
        "Hello",
        "World",
        "I",
        "am",
        "ready."
      ];
    });
    test("gets created with the correct length for given quadratic gridsize",
        () {
      Grid sut = Grid(3, 3);
      expect(sut.length, 9);
    }, tags: const ["happy"]);
    test("gets created with the correct length for given rectangular gridsize",
        () {
      Grid sut = Grid(87, 85);
      expect(sut.length, 7395);
    }, tags: const ["happy"]);
    group(".from", () {
      test("copies the content of another grid on .from Constructor call", () {
        Grid original = Grid(2, 2);
        original[0] = "Hey";
        original[1] = "you";
        original[2] = "me";
        original[3] = "together";

        Grid sut = Grid.from(original);
        expect(sut, containsAllInOrder(["Hey", "you", "me", "together"]));
      }, tags: const ["happy"]);
      test("copies the length of another grid on .from Constructor call", () {
        Grid original = Grid(2, 2);
        original[0] = "Hey";
        original[1] = "you";
        original[2] = "me";
        original[3] = "together";

        Grid sut = Grid.from(original);
        expect(sut.length, 4);
      }, tags: const ["happy"]);
    });
    group(".fromList", () {
      test("sets the length for list passed in on .fromList Constructor call",
          () {
        Grid sut = Grid.fromList(l, 3);

        expect(sut.length, 9);
      }, tags: const ["happy"]);
      test("sets the contents of list passed in on .fromList Constructor call",
          () {
        Grid sut = Grid.fromList(l, 3);

        expect(sut[3], "together");
      }, tags: const ["happy"]);
      test(
          "sets the correct height for list passed in on .fromList Constructor call",
          () {
        Grid sut = Grid.fromList(l, 3);

        expect(sut.width, 3);
      }, tags: const ["happy"]);
    });
    group(".fill", () {
      test("fills list with results of function passed in", () {
        Grid<String> sut = Grid.fill(3, 3, "testValue");
        expect(
            sut,
            containsAllInOrder([
              "testValue",
              "testValue",
              "testValue",
              "testValue",
              "testValue",
              "testValue",
              "testValue",
              "testValue",
              "testValue"
            ]));
      }, tags: const ["happy"]);
    });
  });
  group("toIndex", () {
    Grid sut;
    setUp(() {
      sut = Grid(3, 3);
    });
    test("throws RangeError on negative x argument", () {
      expect(() => sut.toIndex(-1, 2), throwsA(isRangeError));
    }, tags: const ["bad"]);
    test("throws RangeError on negative y argument", () {
      expect(() => sut.toIndex(2, -1), throwsA(isRangeError));
    }, tags: const ["bad"]);
    test("calculates correct index for first element", () {
      expect(sut.toIndex(0, 0), equals(0));
    }, tags: const ["happy"]);
    test("calculates correct index for last element", () {
      expect(sut.toIndex(2, 2), equals(8));
    }, tags: const ["happy"]);
    test("calculates correct index for element on first row", () {
      expect(sut.toIndex(2, 0), equals(2));
    }, tags: const ["happy"]);
    test("calculates correct index for example element", () {
      expect(sut.toIndex(1, 1), equals(4));
    }, tags: const ["happy"]);
  });
  group("coordinates getter", () {
    Grid sut;
    setUp(() {
      sut = Grid(3, 3);
      sut.setAll(0,
          ["Hey", "you", "me", "together", "Hello", null, "I", "am", "ready."]);
    });
    test("returns null if no element exists at the position requested", () {
      expect(sut.get(2, 1), null);
    }, tags: const ["sad"]);
    test("throws RangeError if requesting element outside of grid width", () {
      expect(() => sut.get(4, 1), throwsRangeError);
    }, tags: const ["bad"]);
    test("throws RangeError if requesting element outside of grid height", () {
      expect(() => sut.get(1, 4), throwsRangeError);
    }, tags: const ["bad"]);
    test("returns element at correct index", () {
      expect(sut.get(1, 0), "you");
    }, tags: const ["happy"]);
    test("returns last element correctly", () {
      expect(sut.get(2, 2), "ready.");
    }, tags: const ["happy"]);
  });
  group("toCoords", () {
    Grid sut;
    setUp(() {
      sut = Grid(3, 3);
    });
    test("throws RangeError on negative index argument", () {
      expect(() => sut.toCoordinates(-1), throwsA(isRangeError));
    }, tags: const ["bad"]);
    test("calculates correct index for first element", () {
      expect(sut.toCoordinates(0), equals(Point(0, 0)));
    }, tags: const ["happy"]);
    test("calculates correct index for last element", () {
      expect(sut.toCoordinates(8), equals(Point(2, 2)));
    }, tags: const ["happy"]);
    test("calculates correct index for last element on first row", () {
      expect(sut.toCoordinates(2), equals(Point(2, 0)));
    }, tags: const ["happy"]);
    test("calculates correct index for example element", () {
      expect(sut.toCoordinates(6), equals(Point(0, 2)));
    }, tags: const ["happy"]);
  });
  group("coordinates setter", () {
    Grid<String> sut;
    setUp(() {
      sut = Grid(3, 3);
      sut.setAll(0,
          ["Hey", "you", "me", "together", "Hello", null, "I", "am", "ready."]);
    });
    test("sets element to null if passing null in", () {
      sut.set(1, 1, null);
      expect(sut.get(1, 1), null);
    }, tags: const ["sad"]);
    test("throws RangeError if setting element outside of grid width", () {
      expect(() => sut.set(4, 1, "testValue"), throwsRangeError);
    }, tags: const ["bad"]);
    test("throws RangeError if setting element outside of grid height", () {
      expect(() => sut.set(1, 4, "testValue"), throwsRangeError);
    }, tags: const ["bad"]);
    test("sets element at correct index", () {
      sut.set(1, 0, "testValue");
      expect(sut.get(1, 0), "testValue");
    }, tags: const ["happy"]);
    test("sets last element correctly", () {
      sut.set(2, 2, "testValue");
      expect(sut.get(2, 2), "testValue");
    }, tags: const ["happy"]);
  });
}
