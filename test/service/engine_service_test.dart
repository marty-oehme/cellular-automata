import 'package:mockito/mockito.dart';
import 'package:rules_of_living/service/engine_service.dart';
import 'package:rules_of_living/src/Engine.dart';
@TestOn('browser')
import 'package:test/test.dart';

class MockEngine extends Mock implements Engine {}

void main() {
  EngineService sut;
  MockEngine me;
  setUp(() {
    me = MockEngine();
    sut = EngineService();
  });
  group("Dependency Injection", () {
    test("EngineService can be passed a custom Engine", () {
      sut.engine = me;

      Engine result = sut.engine;
      expect(result, equals(me));
    });
  });
  group("caching", () {
    test("EngineService creates an engine on demand", () {
      Engine result = sut.engine;
      expect(result, TypeMatcher<Engine>());
    });

    test("EngineService returns the cached engine on subsequent requests", () {
      Engine result = sut.engine;
      expect(sut.engine, equals(result));
    });
    test("caching can be overriden by providing a custom engine", () {
      Engine first = sut.engine;
      sut.engine = me;
      Engine second = sut.engine;
      expect(second, isNot(equals(first)));
    });
  });
}
