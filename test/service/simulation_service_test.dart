import 'package:rules_of_living/service/engine_service.dart';
import 'package:rules_of_living/service/simulation_service.dart';
import 'package:rules_of_living/src/Simulation.dart';
import 'package:test/test.dart';
import 'package:mockito/mockito.dart';

class MockSimulation extends Mock implements Simulation {}

class MockEngineService extends Mock implements EngineService {}

void main() {
  SimulationService sut;
  MockSimulation mockSim = MockSimulation();
  setUp(() => sut = SimulationService(MockEngineService(), mockSim));
  test("calling save calls through to Simulation.saveSnapshot", () {
    sut.save();
    verify(mockSim.saveSnapshot());
  });
  test("calling load calls through to Simulation.loadSnapshot", () {
    sut.load();
    verify(mockSim.loadSnapshot());
  });
}
