import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:rules_of_living/service/engine_service.dart';
import 'package:rules_of_living/service/simulation_service.dart';

@Component(
  selector: 'sim-controls',
  templateUrl: "controls_component.html",
  directives: [
    coreDirectives,
    MaterialButtonComponent,
    MaterialIconComponent,
    MaterialTooltipDirective
  ],
  providers: [],
  styleUrls: const ["controls_component.css"],
)
class ControlsComponent {
  final EngineService engine;
  final SimulationService sim;

  ControlsComponent(this.engine, this.sim);

  void onStartClicked() {
    engine.toggleRunning();
  }

  void onStepClicked() {
    engine.step();
  }

  void onSaveClicked() {
    sim.save();
  }

  void onLoadClicked() {
    sim.load();
  }

  void onRandomClicked() {
    sim.addRandomPattern();
    engine.stop();
  }

  void onClearClicked() {
    sim.reset();
  }
}
