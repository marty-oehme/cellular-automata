import 'dart:html' as html;

import 'package:angular/angular.dart';
import 'package:rules_of_living/service/engine_service.dart';
import 'package:rules_of_living/service/simulation_service.dart';

@Component(
  selector: 'gol-simulation',
  templateUrl: "simulation_component.html",
  directives: [coreDirectives],
  providers: [],
)
class SimulationComponent implements OnInit {
  final EngineService engine;
  final SimulationService sim;

  SimulationComponent(this.engine, this.sim);

  @override
  void ngOnInit() {
    html.CanvasElement canvas = html.CanvasElement()..id = "simulation";
    html.querySelector("#simulation")..append(canvas);
    canvas.width = 500;
    canvas.height = 500;

    canvas.context2D.setFillColorRgb(200, 0, 0);
    canvas.context2D.fillRect(0, 0, canvas.width, canvas.height);
    canvas.context2D.setFillColorRgb(0, 255, 0);
    canvas.context2D.fillText('''
    If you see this
    
    the canvas did not load correctly :(
    ''', canvas.width / 2 - 50, canvas.height / 2);
    sim.canvas = canvas;
  }
}
