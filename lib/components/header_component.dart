import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';

@Component(
  selector: 'app_header',
  templateUrl: "header_component.html",
  directives: [
    coreDirectives,
    MaterialButtonComponent,
    MaterialIconComponent,
    MaterialSliderComponent
  ],
  providers: [],
  styleUrls: const ['package:angular_components/app_layout/layout.scss.css'],
)
class HeaderComponent {}
