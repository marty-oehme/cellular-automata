import 'package:angular/angular.dart';
import 'package:rules_of_living/app_component.template.dart' as ng;


void main() {
  runApp(ng.AppComponentNgFactory);
}
